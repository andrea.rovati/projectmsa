# ProjectMSA repository

This is the repository containing Statistical Methods for Machine Learning's project, academic year 2019/2020
Università degli Studi di Milano
Instructor: Nicolò Cesa-Bianchi

# Instructions

In order to use the project, you have to download the data (which aren't in the repository, follow this [link](https://docs.google.com/document/d/e/2PACX-1vTqeS0WmZDMvxuN8auSIvAVncNg7zSR73Ibz6XaAKOjk7W3QJsAN4j6kJKUZN156f0y1_BUyrgiJSQk/pub)) in the **same** repository as the project itself.
Moreover, most of the plots shown while executing the project are stored in a folder ```report/images``` (which I suppose you have, I don't create itself). You can modify where to save the images changing the  variable ```STORE_IMAGES```. By default, the images will not be saved.
